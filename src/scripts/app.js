// Load jQuery from NPM
import $ from 'jquery';

window.jQuery = $;
window.$ = $;

import menu from "./modules/menu"
import "./modules/video"
import "./modules/stickyheader"
import slide from "./modules/slide"
import gallery from "./modules/gallery"
import mask from "./modules/mask"
import "./modules/map"
import aos from "./modules/animate/aos"
import selectric from "./modules/selectric"
import plants from "./modules/plants"
import bts from "./modules/bts"
import ScrollTo from "./modules/scrollto"
import send from "./modules/send"
//import nprogress from "./modules/nprogress"
import "./modules/spa"
import brz from "../images/brz.svg"

export default class App {
  constructor() {
    console.log(
      "\n%c     ",
      `font-size: 30px; background: url(${window.location.origin}/assets/${brz}) no-repeat;`
    )
    console.log(
      "%cWe love " + "%cto inspect " + "%ccodes too \ud83d\udc40\ud83d\ude42",
      "font-size: 16px; color: #91c43c;",
      "font-size: 16px; color: #fcc738;",
      "font-size: 16px; color: #44a5d3;"
    )

    //new nprogress()
    new menu()
    new slide()
    new selectric()
    new aos()
    new gallery()
    new plants()
    new send()
    new bts()
    new mask()
    ScrollTo()
  }
}

document.addEventListener("DOMContentLoaded", () => new App())
