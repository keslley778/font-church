const extend = function () {
  const extended = {}
  let deep = false
  let i = 0

  if (Object.prototype.toString.call(arguments[0]) === "[object Boolean]") {
    deep = arguments[0]
    i++
  }

  const merge = function (obj) {
    for (const prop in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
        if (
          deep &&
          Object.prototype.toString.call(obj[prop]) === "[object Object]"
        ) {
          extended[prop] = extend(extended[prop], obj[prop])
        } else {
          extended[prop] = obj[prop]
        }
      }
    }
  }

  for (; i < arguments.length; i++) {
    merge(arguments[i])
  }

  return extended
}

export default extend
