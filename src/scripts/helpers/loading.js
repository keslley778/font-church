const loading = (isloading = true, btn) => {
  if (isloading) {
    btn.disabled = true
    btn.style.textAlign = "left"
    btn.insertAdjacentHTML(
      "beforeend",
      '<span class="loading js-loading"></span>'
    )
  } else {
    btn.querySelector(".js-loading").remove()
    btn.style.textAlign = "center"
    btn.disabled = false
  }
}

export default loading
