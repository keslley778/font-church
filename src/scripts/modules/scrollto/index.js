/**
 * Scroll To
 */

const ScrollTo = () => {
  // const toggleElements = document.querySelectorAll(
  //   "body, .js-menu, .js-toggle, .js-header"
  // )
  // const links = document.querySelectorAll(".js-menu .list .link")

  document.querySelectorAll(".js-scroll-to").forEach(scrollLink => {
    scrollLink.addEventListener("click", event => {
      event.preventDefault()
      // links.forEach(link => link.classList.remove("-active"))
      // event.target.classList.add("-active")
      // toggleElements.forEach(el => el.classList.remove("-open"))

      const target = event.target.getAttribute("href")
      const offset =
        document.querySelector(target).getAttribute("data-offset") | 0

      $("html, body").animate(
        {
          scrollTop: $(target).offset().top - offset,
        },
        500
      )
    })
  })
}

export default ScrollTo
