import { TimelineMax, TweenMax, Power1, Power2 } from "gsap/TweenMax"
import { event } from "jquery"

export default class Menu {
  	constructor() {
		
		// Open Menu

		var tlmenu = new TimelineMax({
			paused: true,
			delay: -1,
		})

		tlmenu.staggerFromTo(
			".nav",
			1,
			{ display: "none", height: "0", ease: Power2.easeInOut },
			{ display: "block", height: "100vh", ease: Power1.easeInOut },
			1
		)
		tlmenu.staggerFromTo(
			".menu li",
			0.7,
			{ x: -10, opacity: 0, ease: Power2.easeInOut },
			{ x: 0, opacity: 1, ease: Power2.easeInOut },
			0.1
		)

		// Toggle

		const toggle = document.querySelector(".js-toggle")
		const Body = document.querySelector("body")

		if (toggle) {
			toggle.addEventListener("click", event => {
				event.preventDefault()
				toggle.classList.toggle("is-active")
				Body.classList.toggle("body-menu-open")

				if (toggle.classList.contains('is-active')) {
					tlmenu.play()
				} else{
					tlmenu.reverse()
				}
			})
		}

		const listMenu = document.querySelectorAll(".menu li:not(.dropdown)")
		for (const item of listMenu) {
			item.addEventListener('click', function(event) {
				tlmenu.reverse()
				toggle.classList.remove("is-active")
				Body.classList.remove("body-menu-open")
			})
		}

	}
}
