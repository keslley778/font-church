class StickyHeader {
  constructor() {
    let lastScrollTop = 0

    window.addEventListener(
      "scroll",
      () => {
        const st = window.pageYOffset || document.documentElement.scrollTop

        if (st > lastScrollTop && st > 80) {
          $(".js-sticky").addClass("-scroll")
        } else if (st < 40) {
          $(".js-sticky").removeClass("-scroll")
        } else {
          $(".js-sticky").removeClass("-scroll")
        }
        lastScrollTop = st
      },
      false
    )
  }
}

export const stickyHeader = new StickyHeader()
