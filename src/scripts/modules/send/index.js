import axios from "axios"
import swal from "sweetalert"
import dataAppend from "../../helpers/dataAppend"
import loading from "../../helpers/loading"

export default class Send {
  constructor() {
    const sendButtons = document.querySelectorAll(".js-send-form")
    sendButtons.forEach(sendButton => {
        sendButton.addEventListener("click", event => {
            handleOnSubmit(event)
        })
    })
  }
}

const handleOnSubmit = async event => {
  event.preventDefault()
  loading(true, event.target)

  const form = event.target.closest(".js-form")
  const formData = dataAppend(form)

  try {
    const response = await axios({
      method: "post",
      headers: { "Content-Type": "multipart/form-data" },
      url: form.action,
      data: formData,
    })

    handleSuccess(response.data, form)
  } catch (err) {
    handleError(err.response.data, form)
  } finally {
    loading(false, event.target)
  }
}

const handleSuccess = (response, form) => {
  let timeout = 0
  const { reset, title, message, redirect } = response

  removeValidation(form)

  if (typeof reset === "undefined") {
    form.reset()
  }

  if (title || message) {
    timeout = 3000
    swal({
      title: title || "Obrigado",
      text: message || "Enviado com sucesso!",
      icon: "success",
      timer: timeout,
      closeOnClickOutside: false,
      buttons: false,
    })
  } else {
    timeout = 100
  }

  if (redirect) {
    setTimeout(() => {
      window.location.replace(redirect)
    }, timeout)
  }
}

const handleError = (response, form) => {
  removeValidation(form)
  addValidation(response, form)
}

const addValidation = (response, form) => {
  const { title, message, data } = response

  swal({
    title: title || "Ops",
    text: message || "Ocorreu um erro ao enviar, verifique e tente novamente.",
    icon: "error",
    timer: 2000,
    closeOnClickOutside: false,
    buttons: false,
  })

  if (data) {
    Object.keys(data).forEach((index, key) => {
      // console.log(index)
      const input = form.querySelector(`[name="${index}"]`)

      if (input) {
        const group = input.closest(".js-validate")
        if (!group.classList.contains("-invalid")) {
          group.classList.add("-invalid")
        }

        const messageError = document.createElement("div")
        messageError.classList.add("help-block")
        messageError.appendChild(document.createTextNode(data[index]))

        if (!group.querySelector(".help-block")) {
          group.appendChild(messageError)
        }
      }
    })
  }
}

const removeValidation = form => {
  const groups = form.querySelectorAll(".js-validate")

  groups.forEach(group => {
    if (group.classList.contains("-invalid")) {
      group.classList.remove("-invalid")
    }
    if (group.querySelector(".help-block")) {
      group.removeChild(group.querySelector(".help-block"))
    }
  })
}

//export const send = new Send()
