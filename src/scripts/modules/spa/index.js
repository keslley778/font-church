import NProgress from 'nprogress'
import barba from '@barba/core';
import slide from '../slide/index'
import menu from '../menu/index'
import selectric from '../selectric/index'
import gallery from '../gallery/index'
import aos from '../animate/aos'
import plants from '../plants/index'
import send from '../send/index'
import mask from '../mask/index'

class Spa {
    constructor() {
        const Body = document.querySelector("body")

        // barba js

        barba.hooks.before(() => {
            Body.classList.add("is-transition")
            NProgress.start()
        })
        
        barba.hooks.after(() => {
            Body.classList.remove("is-transition")
            new menu()
            new slide()
            new selectric()
            new aos()
            new gallery()
            new plants()
            new mask()
            new send()
            NProgress.done()
        })
        
        barba.hooks.enter(() => {
            window.scrollTo(0, 0)
        })

        function delay(n) {
            n = n || 2000;
            return new Promise((done) => {
                setTimeout(() => {
                    done()   
                }, n)
            })
        }

        barba.init({
            sync: true,
            transitions: [{
                name: 'transitionDefault',
                async leave(data) {
                    const done = this.async();
                    await delay(1000);
                    done();
                },
                async enter(data) {
                    
                },
            }]
        })

    }
}
  
export const spa = new Spa()
  