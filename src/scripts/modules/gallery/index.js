export default class Gallery {
  constructor() {
    $(".js-gallery").lightGallery({
      selector: ".js-gallery-photo",
      mode: "lg-fade",
      speed: 400,
      loop: false,
      thumbnail: false,
      download: false,
      zoom: false,
      fullScreen: false,
      share: false,
      getCaptionFromTitleOrAlt: false,
      subHtmlSelectorRelative: false,
      autoplayControls: false,
      closable: true,
      hideBarsDelay: 200000
    })
  }
}