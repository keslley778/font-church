/* eslint-disable no-undef */
import axios from "axios"
import extend from "../../helpers/extend"
import boolParse from "../../helpers/bool"

const googleMaps = (elements, options) => {
  return elements.forEach(el => {
    el.classList.add("map")

    const params = {
      api: el.dataset.api || null,
      zoom: parseInt(el.dataset.zoom) || 21,
      scroll: boolParse(el.dataset.scroll) || false,
      lat: el.dataset.lat || null,
      lng: el.dataset.lng || null,
      title: el.dataset.title || null,
      address: el.dataset.address || null,
      description: el.dataset.description || null,
      image: el.dataset.image || null,
      icon: el.dataset.icon || null,
      defaultPosition: el.dataset.defaultPosition || "-7.108270,-34.830408",
    }

    const defaultPosition = params.defaultPosition.split(",")

    const settings = extend(
      {
        fullscreenControl: true,
        scaleControl: false,
        streetViewControl: false,
        scrollwheel: params.scroll,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        mapTypeControl: false,
        center: new google.maps.LatLng(defaultPosition[0], defaultPosition[1]),
        zoom: params.zoom,
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL,
          position: google.maps.ControlPosition.TOP_RIGHT,
        },
      },
      options
    )

    const map = new google.maps.Map(el, settings)
    const bounds = new google.maps.LatLngBounds()
    const openCard = []

    if (params.api === null) {
      if (params.lat !== null && params.lng !== null) {
        const location = params
        pushToMap(map, bounds, params, location, openCard)
      }
    } else {
      fetchLocations(params.api, map, bounds, params, openCard)
    }
  })
}

const normalizeData = data => {
  return {
    title: data.title || null,
    address: data.address || null,
    description: data.description || null,
    lat: data.lat || null,
    lng: data.lng || null,
    image: data.image || null,
    icon: data.icon || null,
  }
}

const fetchLocations = async (endpoint, map, bounds, params, openCard) => {
  try {
    const response = await axios.get(endpoint)

    response.data.locations.forEach(data => {
      const location = normalizeData(data)
      pushToMap(map, bounds, params, location, openCard)
    })
  } catch (err) {
    console.error("Error on fetch locations.")
  }
}

const pushToMap = (map, bounds, params, location, openCard) => {
  const data = {}
  let icon = location.icon

  if (icon === null) {
    icon = params.icon
  }

  if (icon !== null) {
    data.icon = new google.maps.Marker({
      url: icon,
      size: new google.maps.Size(28, 37),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(18, 34),
      scaledSize: new google.maps.Size(28, 37),
    })
  }

  data.position = new google.maps.LatLng(location.lat, location.lng)
  data.content = createCard(location)

  const markerSettings = extend(
    {
      map: map,
    },
    data
  )

  const marker = new google.maps.Marker(markerSettings)

  bounds.extend(data.position)
  map.panToBounds(bounds)

  handleOnClick(map, bounds, marker, data.content, openCard)
}

const createCard = location => {
  const card = document.createElement("div")
  card.setAttribute("class", "map-card")

  if (location.image !== null) {
    card.classList.add("-with-image")
    const figure = document.createElement("figure")
    figure.setAttribute("class", "map-figure")

    const image = document.createElement("img")
    image.setAttribute("class", "map-image img-cover")
    image.setAttribute("src", location.image)

    figure.appendChild(image)
    card.appendChild(figure)
  }

  const caption = document.createElement("div")
  caption.setAttribute("class", "map-caption")

  if (location.title) {
    const title = document.createElement("h5")
    title.setAttribute("class", "map-title")
    title.appendChild(document.createTextNode(location.title))
    caption.appendChild(title)
  }

  if (location.address) {
    const address = document.createElement("address")
    address.setAttribute("class", "map-address")
    address.appendChild(document.createTextNode(location.address))
    caption.appendChild(address)
  }

  if (location.description) {
    const description = document.createElement("p")
    description.setAttribute("class", "map-description")
    description.appendChild(document.createTextNode(location.description))
    caption.appendChild(description)
  }

  if (location.lat && location.lng) {
    const link = document.createElement("a")
    link.setAttribute("class", "map-link icon-pin-before")
    link.setAttribute("target", "_blank")
    link.setAttribute("title", "Como chegar")
    link.setAttribute(
      "href",
      `https://www.google.com.br/maps/dir//${location.lat},${location.lng}`
    )
    link.appendChild(document.createTextNode("Como chegar"))
    caption.appendChild(link)
  }

  card.appendChild(caption)

  return card
}

const handleOnClick = (map, bounds, marker, data, openCard) => {
  const infoWindow = new google.maps.InfoWindow({
    content: data,
  })

  marker.addListener("click", () => {
    if (openCard[0]) {
      openCard[0].close()
      openCard.pop()
    }

    infoWindow.open(map, marker)
    openCard.push(infoWindow)
    // map.panToBounds(bounds);
  })
}

export default googleMaps
