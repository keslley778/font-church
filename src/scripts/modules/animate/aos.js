import AOS from "aos"

export default class AOSAnimate {
  constructor() {
    AOS.init({
      once: false,
      // disable: "mobile",
    })
  }
}
