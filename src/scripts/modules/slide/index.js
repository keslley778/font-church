import Swiper from "swiper"
import boolParse from "../../helpers/bool"
import slides from "./slides"

export default class Slide {
  constructor() {
    document.querySelectorAll(".js-slide").forEach(async (slide, index) => {
      const settings = {
        mobileBreak: slide.dataset.mobileBreak || 767,
        onlyMobile: slide.hasAttribute("data-only-mobile")
          ? boolParse(slide.dataset.onlyMobile)
          : false,
        simulateTouch: slide.hasAttribute("data-simulate-touch")
          ? boolParse(slide.dataset.simulateTouch)
          : false,
        autoplay: slide.hasAttribute("data-autoplay")
          ? boolParse(slide.dataset.autoplay)
          : false,
        loop: slide.hasAttribute("data-loop")
          ? boolParse(slide.dataset.loop)
          : false,
        autoHeight: slide.hasAttribute("data-auto-height")
          ? boolParse(slide.dataset.autoHeight)
          : false,
        parallax: slide.hasAttribute("data-parallax")
          ? boolParse(slide.dataset.parallax)
          : false,
        centeredSlides: slide.hasAttribute("data-centered-slides")
          ? boolParse(slide.dataset.centeredSlides)
          : false,
        speed: parseInt(slide.dataset.speed) || 400,
        perColumn: parseInt(slide.dataset.perColumn) || 1,
        perColumnMD: parseInt(slide.dataset.perColumnMd) || 1,
        perColumnXS: parseInt(slide.dataset.perColumnXs) || 1,
        perView: parseInt(slide.dataset.perView) || 1,
        perViewXL: parseInt(slide.dataset.perViewXl) || 1,
        perViewLG: parseInt(slide.dataset.perViewLg) || 1,
        perViewMD: parseInt(slide.dataset.perViewMd) || 1,
        perViewSM: parseInt(slide.dataset.perViewSm) || 1,
        perViewXS: parseInt(slide.dataset.perViewXs) || 1,
        perViewXXS: parseInt(slide.dataset.perViewXXs) || 1,
        spaceBetween: parseInt(slide.dataset.spaceBetween) || 0,
        effect: slide.dataset.effect || "slide", // "slide", "fade", "cube", "coverflow", "flip"
        pagination: slide.dataset.pagination || "bullets", // "bullets", "fraction", "progressbar",
        direction: slide.dataset.direction || "horizontal",
      }

      slide.classList.add(`slide-${index}`)

      if (slide.querySelector(".slide-pagination")) {
        slide
          .querySelector(".slide-pagination")
          .classList.add(`pagination-${index}`)
      }
      if (slide.querySelector(".slide-prev")) {
        slide.querySelector(".slide-prev").classList.add(`prev-${index}`)
      }
      if (slide.querySelector(".slide-next")) {
        slide.querySelector(".slide-next").classList.add(`next-${index}`)
      }

      if (settings.onlyMobile === true) {
        slide.classList.add("swiper-only-mobile")

        if (window.outerWidth <= settings.mobileBreak) {
          this.create(slide, index, settings)
        }

        await this.handleOnResize(slide, index, settings)
        await this.updateSlide(index)
      } else {
        await this.create(slide, index, settings)
        await this.updateSlide(index)
      }
    })
  }

  create(slide, index, settings) {
    slide.classList.add("swiper")
    slide.querySelector(".slide-wrapper").classList.add("swiper-wrapper")

    slide.querySelectorAll(".slide-item").forEach(item => {
      item.classList.add("swiper-slide")
    })

    slides[index] = new Swiper(`.slide-${index}`, {
      pagination: {
        el: `.pagination-${index}`,
        type: settings.pagination,
        clickable: true,
      },
      navigation: {
        // nextEl: `.next-${index}`,
        // prevEl: `.prev-${index}`
        nextEl: ".slide-next",
        prevEl: ".slide-prev",
      },
      autoHeight: settings.autoHeight,
      direction: settings.direction,
      loop: settings.loop,
      autoplay: settings.autoplay,
      slideClass: "swiper-slide",
      wrapperClass: "swiper-wrapper",
      spaceBetween: settings.spaceBetween,
      slidesPerColumn: settings.perColumn,
      slidesPerView: settings.perView,
      centeredSlides: settings.centeredSlides,
      effect: settings.effect,
      simulateTouch: settings.simulateTouch,
      parallax: settings.parallax,
      observer: true,
      speed: settings.speed,
      breakpoints: {
        2600: {
          slidesPerView: settings.perViewXL,
        },
        1599: {
          slidesPerView: settings.perViewLG,
        },
        1199: {
          slidesPerView: settings.perViewMD,
          slidesPerColumn: settings.perColumnMD,
        },
        991: {
          slidesPerView: settings.perViewSM,
        },
        767: {
          slidesPerView: settings.perViewXS,
          slidesPerColumn: settings.perColumnXS,
        },
        540: {
          slidesPerView: settings.perViewXXS,
          slidesPerColumn: settings.perColumnXXS,
        },
      },
    })
  }

  updateSlide(index) {
    slides[index].update()
  }

  handleOnResize(slide, index, settings) {
    window.addEventListener("resize", () => {
      if (window.innerWidth <= settings.mobileBreak && !slides[index]) {
        this.create(slide, index, settings)
      } else if (window.innerWidth >= settings.mobileBreak + 1) {
        slide.classList.remove("swiper")
        if (slide.classList.contains("swiper-wrapper")) {
          slide.classList.remove("swiper-container")
        }

        const wrapper = slide.querySelector(".slide-wrapper")

        if (wrapper.classList.contains("swiper-wrapper")) {
          wrapper.classList.remove("swiper-wrapper")
        }

        const items = slide.querySelectorAll(".slide-item")

        items.forEach(item => {
          if (item.classList.contains("swiper-slide")) {
            item.classList.remove("swiper-slide")
          }
        })

        if (this.items[index]) {
          this.items[index].destroy(false, true)
          this.items[index] = undefined
        }
      }
    })
  }
}