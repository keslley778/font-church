class Video {
  constructor() {
    $(".video").each(function () {
      $("body").on("click", ".open-video", function () {
        const value = $(this).attr("data-src-video")
        const src = "https://www.youtube.com/embed/" + value + "?rel=0"
        $(".frame-video").attr("src", src)
        setTimeout(function () {
          $(".mask-video").fadeOut()
        }, 800)
      })
      $("body").on("click", ".modal-open, .close-video", function () {
        $(".mask-video").fadeIn()
      })
    })
  }
}

export const video = new Video()
