// JQUERY
import $ from "jquery"

const JQuery = () => {
  window.jQuery = $
  window.$ = $
}

export const jquery = new JQuery()
