export default class Plants {
    constructor() {
        $(".box-plants").each(function () {
            $("body").on("click", ".btn-plant", function () {
            var tabId = $(this).attr("data-tab")

            $(".btn-plant").removeClass("-active")
            $(".tab-content").removeClass("-active")

            $(this).addClass("-active")
                $("#" + tabId).addClass("-active")
            })
        })
    }
}

