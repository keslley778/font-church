const path = require("path")

let ROOT = process.env.PWD

if (!ROOT) {
  ROOT = process.cwd()
}

const settings = {
  appName: "fonte-church",
  favicon: path.join(ROOT, "/src/images/favicon.svg"),
  googleMapsKey: "AIzaSyD2n_mkFxhqGL8Bdu1RfTl4FXoGcr9zjv4",
  devHost: "localhost",
  port: process.env.PORT || 8000,
  env: process.env.NODE_ENV,
  root: ROOT,
  paths: {
    config: "config",
    src: "src",
    dist: "../public/assets",
  },
}

module.exports = settings
